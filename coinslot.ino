/*
Coinslot Liquid Dispenser Machine
v.1.0

Copyright 2024 Rogey A. Miedes

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


SOURCES:
Coin Slot:
https://projecthub.arduino.cc/mdraber/control-coin-acceptor-with-arduino-36e871

Debouncing Button Switch:
https://www.programmingelectronics.com/debouncing-a-button-with-arduino/
*/

#include  <Arduino.h>
/* TODO:
1. Set a maximum number of coins to insert.
   Reject coins if over maximum.

2. Add LCD panel.

*/

// -------------------------  START of INITIALIZATIONS
enum machineState {
  IDLE,
  COINS_COUNTING,
  DISPENSING,
};

#define N_OPEN 0
#define N_CLOSE 1

// State checker
// int loop_counter = 0;
int old_amount = 0;
int steady_state_count = 0;
static const int steady_state_target = 10;
static bool is_steady_state = false;


// Pin layouts
const int P_AMOUNT_COUNTER = 2;
const int P_INDICATOR = 13;

// relay
const int RELAY_pin = 7;

// Coin slot
volatile int amount_count=0; // Number of impulses detected
float total_amount=0; // // Sum of all the  coins inserted

machineState state = IDLE;

// momentary switch
int MSWITCH_pin = 4;
int MSWITCH_lastdtime = 0;
static const int MSWITCH_DTDELAY = 1000;

// timer
long TIMER_start = 0;
long TIMER_diff = 0;
long TIMER_duration = 0;

static const float UNIT_COST = 2000; // milliseconds per peso

// -----------------------------  END OF INITIALIZATIONS


void setup() {
  state = IDLE; // set initial state to idle;

// OUTPUT PINS
  pinMode(P_INDICATOR, OUTPUT);
  pinMode(RELAY_pin, OUTPUT);
  digitalWrite(RELAY_pin,N_OPEN);

// INPUT PINS
  pinMode(MSWITCH_pin, INPUT);
  digitalWrite(MSWITCH_pin, LOW);

  // Interrupt connected  to PIN D2
  attachInterrupt(digitalPinToInterrupt(P_AMOUNT_COUNTER),incomingImpulse, FALLING);
 
  Serial.begin(9600);
}

void incomingImpulse() {
  if (state == COINS_COUNTING){
   amount_count = amount_count + 1;
  };
}

void loop() {
  // loop_counter = loop_counter + 1;
  total_amount = amount_count;

// States Management
  if (old_amount == total_amount && total_amount != 0){
    steady_state_count = steady_state_count + 1;
    } else { steady_state_count = 0; }; // reset. We need to ensure consecutive steady states across loops to be is_steady_state.

  if (steady_state_count >= steady_state_target){ is_steady_state = true; } else { is_steady_state = false; }; 

  if (state == COINS_COUNTING){
    Serial.print("Total amount: ");
    Serial.print(total_amount);
    Serial.print(".");
    
    if (is_steady_state) { 
      Serial.print(" <Press button to start dispensing.>");
    }

    Serial.println();

  } else if (state == DISPENSING){
    Serial.print("Dispensing your purchase. Total Amount: ");
    Serial.print(total_amount);
    Serial.println();

  } else if (state == IDLE){
    Serial.print(" <Press button to start your purchase.>");   
    Serial.println();
  };

  // Switching States
  // listen to MSWITCH button presses; with debounce
  if (millis() - MSWITCH_lastdtime > MSWITCH_DTDELAY){
    if (digitalRead(MSWITCH_pin) == HIGH){
      MSWITCH_lastdtime = millis();

      if (state == COINS_COUNTING && is_steady_state){
        startDispensing();
      } else if (state == IDLE){
        startCountingCoins();
      };
    };
  };
  
  delay(100);
  old_amount = total_amount; // state-watching
}

void startCountingCoins() {
  Serial.println( "Insert coins up to desired amount.");
  state = COINS_COUNTING;
  digitalWrite(RELAY_pin, N_OPEN);
}

void startDispensing() {
  Serial.println("Starting to dispense your purchase.");
  state = DISPENSING;

  TIMER_duration = total_amount * UNIT_COST;
  TIMER_start = millis();

  digitalWrite(RELAY_pin, N_CLOSE);

  do {
    TIMER_diff = millis() - TIMER_start;
    Serial.print(" Dispensing: ");
    Serial.print(TIMER_diff/UNIT_COST);
    Serial.println();
  } while (TIMER_diff < TIMER_duration);
  
  goIdle();
}



void goIdle() {
  Serial.print(" Going idle...");
  state = IDLE;
  digitalWrite(RELAY_pin, N_OPEN);

  // resetting values
  amount_count = 0;
  total_amount = 0;
  is_steady_state = false;
  steady_state_count = 0;
}





